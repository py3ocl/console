#!/usr/bin/env python

# Copyright 2019 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

## A class to handle 4-bit console colours codes.
class Colour4bit:

    FOREGROUND_BLACK    = 30
    FOREGROUND_RED      = 31
    FOREGROUND_GREEN    = 32
    FOREGROUND_YELLOW   = 33
    FOREGROUND_BLUE     = 34
    FOREGROUND_MAGENTA  = 35
    FOREGROUND_CYAN     = 36
    FOREGROUND_WHITE    = 37

    FOREGROUND_FIRST    = FOREGROUND_BLACK
    FOREGROUND_LAST     = FOREGROUND_WHITE

    BACKGROUND_BLACK    = 40
    BACKGROUND_RED      = 41
    BACKGROUND_GREEN    = 42
    BACKGROUND_YELLOW   = 43
    BACKGROUND_BLUE     = 44
    BACKGROUND_MAGENTA  = 45
    BACKGROUND_CYAN     = 46
    BACKGROUND_WHITE    = 47

    BACKGROUND_FIRST    = BACKGROUND_BLACK
    BACKGROUND_LAST     = BACKGROUND_WHITE

    STYLE_NORMAL        = 0
    STYLE_BOLD          = 1
    STYLE_FAINT         = 2
    STYLE_ITALIC        = 3
    STYLE_UNDERLINE     = 4
    STYLE_SLOW_BLINK    = 5
    STYLE_FAST_BLINK    = 6
    STYLE_CROSSED_OUT   = 9
    STYLE_UNDERLINE_OFF = 24
    STYLE_BLINK_OFF     = 25

    FOREGROUND_COLOURS = [FOREGROUND_BLACK,
                          FOREGROUND_RED,
                          FOREGROUND_GREEN,
                          FOREGROUND_YELLOW,
                          FOREGROUND_BLUE,
                          FOREGROUND_MAGENTA,
                          FOREGROUND_CYAN,
                          FOREGROUND_WHITE]

    BACKGROUND_COLOURS = [BACKGROUND_BLACK,
                          BACKGROUND_RED,
                          BACKGROUND_GREEN,
                          BACKGROUND_YELLOW,
                          BACKGROUND_BLUE,
                          BACKGROUND_MAGENTA,
                          BACKGROUND_CYAN,
                          BACKGROUND_WHITE]

    COLOUR_NAMES = ["BLACK", "RED", "GREEN", "YELLOW", "BLUE", "MAGENTA", "CYAN", "WHITE"]

    ## Initialize with foreground, background and styles.
    def __init__(self, foreground = None, background = None, styles = [], terminate = True, debug = False):
        self.foreground = foreground
        self.background = background
        self.styles = styles
        self.terminate = terminate
        self.debug = debug

    ## Get the codes that precede console text for colouring and stying.
    def GetCodes(self):
        fg_str = str(self.foreground) if self.foreground else ""
        sep = ";" if fg_str else ""
        bg_str = "{}{}".format(sep, str(self.background)) if self.background else ""
        style_str = ""
        for style in self.styles:
            sep = ";" if fg_str or bg_str else ""
            if style:
                style_str += "{}{}".format(sep, str(style))
            sep = ";"
        has_codes = fg_str or bg_str or style_str
        esc = self.GetEscape()
        return "{}[{}{}{}m".format(esc, fg_str, bg_str, style_str) if has_codes else ""

    ## Get "\x1b" or "ESC" when debugging.
    def GetEscape(self):
        return "ESC" if self.debug else "\x1b"

    ## Get terminating string for colouring text.
    def GetEnd(self):
        esc = self.GetEscape()
        return "{}[0m".format(esc)

    ## Get the codes, text and end code as a string.
    def GetText(self, text):
        return "{}{}{}".format(self.GetCodes(), text, self.GetEnd() if self.terminate else "")

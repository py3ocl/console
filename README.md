# console

Python classes to help with Console use, such as handling console colours.

To enable console colours on Windows, do the following:
Using registry editor add DWORD key VirtualTerminalLevel with value 1 to Computer\HKEY_CURRENT_USER\Console

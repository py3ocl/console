#!/usr/bin/env python

# Copyright 2019 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
from RGB import RGB
from Colour4bit import Colour4bit
from Colour8bit import Colour8bit
from Colour24bit import Colour24bit

## A class to handle 4-bit, 8-bit or 24-bit console coloured text.
class ConsoleText:

    ## Return text with colour codes for console.
    ## When terminate is True the returned string as colour codes to restore
    ## colouring to defaults.
    @staticmethod
    def GetText(text, colour):
        return colour.GetText(text)

def Print4bit(terminate, debug):
    print(Colour4bit().GetText("4-BIT DEFAULT"))

    for style in [None, Colour4bit.STYLE_BOLD]:
        for b in Colour4bit.BACKGROUND_COLOURS:
            line = ""
            for f, name in zip(Colour4bit.FOREGROUND_COLOURS, Colour4bit.COLOUR_NAMES):
                c = Colour4bit(f, b, [style], terminate = terminate, debug = debug)
                line += ConsoleText.GetText(" {} ".format(name), c)
            print(line)

def Print8bit(terminate, debug):
    # Print first 16 main colours.
    b = 0
    while b < 16:
        f = 0
        line = ""
        while f < 16:
            c = Colour8bit(f, b, terminate = terminate, debug = debug)
            line += ConsoleText.GetText(" {} ({}) ".format(str(f).rjust(2, '0'), str(b).rjust(2, '0')), c)
            f += 1
        b += 1
        print(line)

    # Print sample colours from 16 to 231.
    b = 16
    while b < 232:
        f = 16
        line = ""
        while f < 232:
            c = Colour8bit(f, b, terminate = terminate, debug = debug)
            line += ConsoleText.GetText(" {} ({}) ".format(str(f).rjust(3, '0'), str(b).rjust(3, '0')), c)
            f += 10
        b += 10
        print(line)

    ## Print 8-bit grey scale shades.
    b = 232
    while b < 256:
        f = 232
        line = ""
        while f < 256:
            c = Colour8bit(f, b, terminate = terminate, debug = debug)
            line += ConsoleText.GetText(" {} ({}) ".format(f, b), c)
            f += 1
        b += 1
        print(line)

    if not debug and not terminate:
        f = Colour4bit.BACKGROUND_WHITE
        line = ConsoleText.GetText("".format(name), Colour4bit(f, None, [style], terminate = True, debug = False))
        print(line)

def Print24Bit(terminate, debug):
    # Print sample of 24-bit grey scale colours.
    b = 0
    while b < 256:
        f = 0
        line = ""
        while f < 256:
            fg_rgb = RGB(f, f, f)
            bg_rgb = RGB(b, b, b)
            c = Colour24bit(fg_rgb, bg_rgb, terminate = terminate, debug = debug)
            line += ConsoleText.GetText(" {} ({}) ".format(str(f).rjust(3, '0'), str(b).rjust(3, '0')), c)
            f += 16
        b += 16
        print(line)

    # Print sample of 24-bit red colours.
    b = 0
    while b < 256:
        f = 0
        line = ""
        while f < 256:
            fg_rgb = RGB(f, 0, 0)
            bg_rgb = RGB(b, 0, 0)
            c = Colour24bit(fg_rgb, bg_rgb, terminate = terminate, debug = debug)
            line += ConsoleText.GetText(" {} ({}) ".format(str(f).rjust(3, '0'), str(b).rjust(3, '0')), c)
            f += 16
        b += 16
        print(line)

    # Print sample of 24-bit green colours.
    b = 0
    while b < 256:
        f = 0
        line = ""
        while f < 256:
            fg_rgb = RGB(0, f, 0)
            bg_rgb = RGB(0, b, 0)
            c = Colour24bit(fg_rgb, bg_rgb, terminate = terminate, debug = debug)
            line += ConsoleText.GetText(" {} ({}) ".format(str(f).rjust(3, '0'), str(b).rjust(3, '0')), c)
            f += 16
        b += 16
        print(line)

    # Print sample of 24-bit blue colours.
    b = 0
    while b < 256:
        f = 0
        line = ""
        while f < 256:
            fg_rgb = RGB(0, 0, f)
            bg_rgb = RGB(0, 0, b)
            c = Colour24bit(fg_rgb, bg_rgb, terminate = terminate, debug = debug)
            line += ConsoleText.GetText(" {} ({}) ".format(str(f).rjust(3, '0'), str(b).rjust(3, '0')), c)
            f += 16
        b += 16
        print(line)

    # Print sample of 24-bit yellow colours.
    b = 0
    while b < 256:
        f = 0
        line = ""
        while f < 256:
            fg_rgb = RGB(f, f, 0)
            bg_rgb = RGB(b, b, 0)
            c = Colour24bit(fg_rgb, bg_rgb, terminate = terminate, debug = debug)
            line += ConsoleText.GetText(" {} ({}) ".format(str(f).rjust(3, '0'), str(b).rjust(3, '0')), c)
            f += 16
        b += 16
        print(line)

    # Print sample of 24-bit purple colours.
    b = 0
    while b < 256:
        f = 0
        line = ""
        while f < 256:
            fg_rgb = RGB(f, 0, f)
            bg_rgb = RGB(b, 0, b)
            c = Colour24bit(fg_rgb, bg_rgb, terminate = terminate, debug = debug)
            line += ConsoleText.GetText(" {} ({}) ".format(str(f).rjust(3, '0'), str(b).rjust(3, '0')), c)
            f += 16
        b += 16
        print(line)

    # Print sample of 24-bit cyan colours.
    b = 0
    while b < 256:
        f = 0
        line = ""
        while f < 256:
            fg_rgb = RGB(0, f, f)
            bg_rgb = RGB(0, b, b)
            c = Colour24bit(fg_rgb, bg_rgb, terminate = terminate, debug = debug)
            line += ConsoleText.GetText(" {} ({}) ".format(str(f).rjust(3, '0'), str(b).rjust(3, '0')), c)
            f += 16
        b += 16
        print(line)

if __name__ == "__main__":

    debug = "--debug" in sys.argv
    terminate = False if "--terminate-off" in sys.argv else True

    # Helper flags for debugging colours.

    Print4bit(terminate, debug)
    Print8bit(terminate, debug)
    Print24Bit(terminate, debug)

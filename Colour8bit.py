#!/usr/bin/env python

# Copyright 2019 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

## A class to handle 8-bit console colours codes.
class Colour8bit:

    BLACK       = 0
    DARK_RED    = 1
    DARK_GREEN  = 2
    DARK_YELLOW = 3
    DARK_BLUE   = 4
    NAVY        = 4
    PURPLE      = 5
    DARK_AQUA   = 6
    TEAL        = 6
    SILVER      = 7
    GREY        = 8
    RED         = 9
    GREEN       = 10
    YELLOW      = 11
    BLUE        = 12
    FUCHSIA     = 13
    AQUA        = 14
    WHITE       = 15

    ## Set foreground and background 8-bit codes, and optionally the terminate and debug flags.
    def __init__(self, foreground = None, background = None, terminate = True, debug = False):
        self.foreground = foreground
        self.background = background
        self.terminate = terminate
        self.debug = debug

    ## Convert foreground, background, terminate and debug values to control codes for terminal.
    def GetCodes(self):
        fg_str = "{}[38;5;{}m".format(self.GetEscape(), self.foreground) if self.foreground else ""
        bg_str = "{}[48;5;{}m".format(self.GetEscape(), self.background) if self.background else ""
        has_codes = fg_str or bg_str
        esc = self.GetEscape()
        return "{}{}".format(fg_str, bg_str) if has_codes else ""

    ## Get "\x1b" or "ESC" when debugging.
    def GetEscape(self):
        return "ESC" if self.debug else "\x1b"

    ## Get terminating string for colouring text.
    def GetEnd(self):
        esc = self.GetEscape()
        return "{}[0m".format(esc)

    ## Get the codes, text and end code as a string.
    def GetText(self, text):
        return "{}{}{}".format(self.GetCodes(), text, self.GetEnd() if self.terminate else "")

#!/usr/bin/env python

# Copyright 2019 Colin Girling
#
 # Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

class Colour24bit:

    def __init__(self, foreground_rgb = None, background_rgb = None, terminate = True, debug = False):
        self.foreground = foreground_rgb
        self.background = background_rgb
        self.terminate = terminate
        self.debug = debug

    @staticmethod
    def GetRGBCodes(colour):
        return "{};{};{}".format(colour.r, colour.g, colour.b)

    def GetCodes(self):
        esc = self.GetEscape()
        if self.foreground:
            fg_rgb = Colour24bit.GetRGBCodes(self.foreground)
            fg_str = "{}[38;2;{}m".format(esc, fg_rgb)
        else:
            fg_str = ""

        if self.background:
            bg_rgb = Colour24bit.GetRGBCodes(self.background)
            bg_str = "{}[48;2;{}m".format(esc, bg_rgb)
        else:
            bg_str = ""
        has_codes = fg_str or bg_str
        return "{}{}".format(fg_str, bg_str) if has_codes else ""

    ## @brief Get "\x1b" or "ESC" when debugging.
    def GetEscape(self):
        return "ESC" if self.debug else "\x1b"

    ## @brief Get terminating string for colouring text.
    def GetEnd(self):
        esc = self.GetEscape()
        return "{}[0m".format(esc)

    ## @brief Get the codes, text and end code as a string.
    def GetText(self, text):
        return "{}{}{}".format(self.GetCodes(), text, self.GetEnd() if self.terminate else "")
